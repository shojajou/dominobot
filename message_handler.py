import telebot
from elements import *
import re
from database import session
from models import State, Customer, Product, Order
import redis

TOKEN = "526827668:AAFe8YR-nUPET3Wh9WedsCseIoiR7FHr0-w"
bot = telebot.TeleBot(TOKEN)

global msg_key, usr_dat
msg_key = 0

global usr_dat
usr_dat = {}

red = redis.Redis('127.0.0.1', port=6379)


@bot.message_handler(commands=["start", "restart"], content_types=['text'])
def start_command(message):
    global usr_dat
    cht_id = message.chat.id
    msg_txt = message.text
    usr_dat[message.chat.id] = {}

    bot.send_message(chat_id=cht_id,
                     text=msg_kyb[1]["message"],
                     reply_markup=msg_kyb[1]["keyboard"])

    red.delete(cht_id)


@bot.message_handler(func=lambda message: message.text == "آغاز دوباره")
def restart_command(message):
    global usr_dat
    cht_id = message.chat.id
    msg_txt = message.text
    usr_dat[message.chat.id] = {}

    bot.send_message(chat_id=cht_id,
                     text=msg_kyb[1]["message"],
                     reply_markup=msg_kyb[1]["keyboard"])

    red.delete(cht_id)


@bot.message_handler(commands=["products"])
def start_command_1(message):
    global usr_dat
    usr_dat[message.chat.id] = {}

    bot.send_message(chat_id=message.chat.id,
                     text=msg_kyb[1]["message"],
                     reply_markup=msg_kyb[1]["keyboard"])


@bot.message_handler(content_types=["text"])
def chk_msg(message):
    global msg_key, usr_dat
    cht_id = message.chat.id
    msg_txt = message.text

    # if msg_txt == "کالاها":
    #     msg_key = 1
    #     bot.send_message(chat_id=cht_id,
    #                      text=msg_kyb[msg_key]["message"],
    #                      reply_markup=msg_kyb[msg_key]["keyboard"])
    #     usr_dat[message.chat.id] = {}

    if msg_txt in rep_mrk_1_keys:
        msg_key = 2
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_key]["message"],
                         reply_markup=msg_kyb[msg_key]["keyboard"])
        usr_dat[cht_id]["product"] = msg_txt

    elif msg_txt in rep_mrk_2_keys:
        msg_key = 3
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_key]["message"],
                         reply_markup=msg_kyb[msg_key]["keyboard"])
        usr_dat[cht_id]["amount"] = msg_txt

    elif msg_txt == "بازگشت":
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_kyb[msg_key]["prev_key"]]["message"],
                         reply_markup=msg_kyb[msg_kyb[msg_key]["prev_key"]]["keyboard"])
        msg_key -= 1

    elif msg_txt == "آغاز دوباره" or msg_txt == '/start':
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[1]["message"],
                         reply_markup=msg_kyb[1]["keyboard"])
        red.delete(cht_id)
        msg_key = 0


@bot.message_handler(content_types=["text"])
def get_address(message):
    global msg_key, usr_dat
    cht_id = message.chat.id
    msg_txt = message.text

    if msg_txt == "بازگشت":
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_kyb[msg_key]["prev_key"]]["message"],
                         reply_markup=msg_kyb[msg_kyb[msg_key]["prev_key"]]["keyboard"])
        msg_key -= 1
        bot.register_next_step_handler(message, chk_loc)

    elif msg_txt == "آغاز دوباره" or msg_txt == '/start':
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[1]["message"],
                         reply_markup=msg_kyb[1]["keyboard"])
        red.delete(cht_id)
        msg_key = 0

    else:
        msg_key = 5
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_key]["message"],
                         reply_markup=msg_kyb[msg_key][
                             "keyboard"])
        usr_dat[cht_id]["address"] = msg_txt
        bot.register_next_step_handler(message, get_name)


@bot.message_handler(content_types=["text"])
def get_name(message):
    global msg_key, usr_dat
    cht_id = message.chat.id
    msg_txt = message.text

    if msg_txt == "بازگشت":
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_kyb[msg_key]["prev_key"]]["message"],
                         reply_markup=msg_kyb[msg_kyb[msg_key]["prev_key"]]["keyboard"])
        msg_key -= 1

    elif msg_txt == "آغاز دوباره" or msg_txt == '/start':
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[1]["message"],
                         reply_markup=msg_kyb[1]["keyboard"])
        red.delete(cht_id)
        msg_key = 0

    else:
        msg_key = 6
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_key]["message"],
                         reply_markup=msg_kyb[msg_key][
                             "keyboard"])
        usr_dat[cht_id]["name"] = msg_txt
        bot.register_next_step_handler(message, get_phone)


@bot.message_handler(content_types=["text"])
def get_phone(message):
    global msg_key, usr_dat
    cht_id = message.chat.id
    msg_txt = message.text

    if msg_txt == "بازگشت":
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_kyb[msg_key]["prev_key"]]["message"],
                         reply_markup=msg_kyb[msg_kyb[msg_key]["prev_key"]]["keyboard"])
        msg_key -= 1

    elif msg_txt == "آغاز دوباره" or msg_txt == '/start':
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[1]["message"],
                         reply_markup=msg_kyb[1]["keyboard"])
        red.delete(cht_id)
        msg_key = 0

    else:
        if re.match(r"^[0-9۰-۹]{11}$", msg_txt) is not None:
            msg_key = 7
            bot.send_message(chat_id=cht_id,
                             text=msg_kyb[msg_key]["message"],
                             reply_markup=msg_kyb[msg_key]["keyboard"])
            usr_dat[cht_id]["phone"] = msg_txt
            bot.register_next_step_handler(message, get_day)

        else:
            msg_key = "phone_error"
            bot.send_message(chat_id=cht_id,
                             text=msg_kyb[msg_key]["message"],
                             reply_markup=msg_kyb[msg_key]["keyboard"])
            bot.register_next_step_handler(message, get_phone)


@bot.message_handler(content_types=["text"])
def get_day(message):
    global msg_key, usr_dat
    cht_id = message.chat.id
    msg_txt = message.text

    if message.text == "بازگشت":
        bot.send_message(chat_id=message.chat.id,
                         text=msg_kyb[msg_kyb[msg_key]["prev_key"]]["message"],
                         reply_markup=msg_kyb[msg_kyb[msg_key]["prev_key"]]["keyboard"])
        msg_key -= 1

    elif msg_txt == "آغاز دوباره" or msg_txt == '/start':
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[1]["message"],
                         reply_markup=msg_kyb[1]["keyboard"])
        red.delete(cht_id)
        msg_key = 0

    else:
        msg_key = 8
        bot.send_message(chat_id=message.chat.id,
                         text=msg_kyb[msg_key]["message"],
                         reply_markup=msg_kyb[msg_key]["keyboard"])
        usr_dat[cht_id]["date"] = message.text
        bot.register_next_step_handler(message, get_time_period)


@bot.message_handler(content_types=["text"])
def get_time_period(message):
    global msg_key, usr_dat
    cht_id = message.chat.id
    msg_txt = message.text

    if msg_txt == "بازگشت":
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_kyb[msg_key]["prev_key"]]["message"],
                         reply_markup=msg_kyb[msg_kyb[msg_key]["prev_key"]]["keyboard"])
        msg_key -= 1

    elif msg_txt == "آغاز دوباره" or msg_txt == '/start':
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[1]["message"],
                         reply_markup=msg_kyb[1]["keyboard"])
        msg_key = 0

    else:
        rep_mrk = ReplyKeyboardMarkup
        if msg_txt == "قبل از ظهر":
            rep_mrk = rep_mrk_8_am
            usr_dat[cht_id]["period"] = msg_txt

        elif msg_txt == "بعد از ظهر":
            rep_mrk = rep_mrk_8_pm
            usr_dat[cht_id]["period"] = msg_txt

        bot.send_message(chat_id=cht_id,
                         text="""
                         لطفا محدوده ساعتی دریافت محصول را انتخاب نمایید.
                         
                         @dominoenergybot
                         """,
                         reply_markup=rep_mrk)
        bot.register_next_step_handler(message, get_time)


@bot.message_handler(content_types=["text"])
def get_time(message):
    global msg_key, usr_dat
    cht_id = message.chat.id
    msg_txt = message.text

    if msg_txt == "بازگشت":
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[msg_kyb[msg_key]["prev_key"]]["message"],
                         reply_markup=msg_kyb[msg_kyb[msg_key]["prev_key"]]["keyboard"])
        msg_key -= 1

    elif msg_txt == "آغاز دوباره" or msg_txt == '/start':
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[1]["message"],
                         reply_markup=msg_kyb[1]["keyboard"])
        red.delete(cht_id)
        msg_key = 0

    else:
        msg_key = 9
        try:
            usr_dat[cht_id]["time"] = msg_txt

            bot.send_message(chat_id=cht_id,
                             text=msg_kyb[msg_key]["message"].format(
                                 usr_dat[cht_id]["product"],
                                 usr_dat[cht_id]["amount"],
                                 usr_dat[cht_id]["name"],
                                 usr_dat[cht_id]["phone"],
                                 usr_dat[cht_id]["date"],
                                 usr_dat[cht_id]["time"] + " " + usr_dat[cht_id]["period"],
                                 usr_dat[cht_id]["address"]
                             ),
                             reply_markup=msg_kyb[msg_key]["keyboard"])

        except Exception as e:
            msg_key = "error_data"
            bot.send_message(chat_id=cht_id,
                             text=msg_kyb[msg_key]["message"],
                             reply_markup=msg_kyb[msg_key]["keyboard"])
            msg_key = 0


@bot.message_handler(content_types=["location"])
def chk_loc(message):
    global msg_key, usr_dat
    cht_id = message.chat.id
    long = message.location.longitude
    lati = message.location.latitude
    usr_dat[cht_id]["longitude"] = long
    usr_dat[cht_id]["latitude"] = lati

    msg_key = 4
    bot.send_message(chat_id=cht_id,
                     text=msg_kyb[msg_key]["message"],
                     reply_markup=msg_kyb[msg_key]["keyboard"])
    bot.register_next_step_handler(message, get_address)


@bot.callback_query_handler(lambda call: True)
def accept_order(call):
    global msg_key, usr_dat
    cal_dat = call.data
    cht_id = call.from_user.id

    bot.answer_callback_query(callback_query_id=call.id, text="پیام شما دریافت شد.")

    if cal_dat == "order_accepted":
        msg_key = 10
        red.hmset(cht_id, usr_dat[cht_id])

        rep_mrk = InlineKeyboardMarkup(row_width=1)
        rep_mrk.add(InlineKeyboardButton(text="پرداخت",
                                         url="http://5.9.227.56:8004/request/%s/%s/%s" %
                                             (calculate_price(cht_id), cht_id, call.message.message_id),
                                         callback_data="pay_started"))
        rep_mrk.row(InlineKeyboardButton(text="انصراف", callback_data="order_denied"))

        bot.edit_message_text(text=msg_kyb[msg_key]["message"].format(
            usr_dat[cht_id]["product"],
            usr_dat[cht_id]["amount"],
            usr_dat[cht_id]["name"],
            usr_dat[cht_id]["phone"],
            usr_dat[cht_id]["date"],
            usr_dat[cht_id]["time"] + " " + usr_dat[cht_id]["period"],
            usr_dat[cht_id]["address"],
            calculate_price(cht_id)
        ), chat_id=cht_id,
            message_id=call.message.message_id,
            reply_markup=rep_mrk
        )

    elif cal_dat == "pay_started":
        red.hmset(cht_id, usr_dat[cht_id])

    elif cal_dat == "order_denied":
        msg_key = "cancel_order"
        bot.edit_message_text(message_id=call.message.message_id,
                              chat_id=cht_id,
                              text=msg_kyb[msg_key]["message"],
                              reply_markup=msg_kyb[msg_key]["keyboard"])

    elif cal_dat == "restart":
        bot.send_message(chat_id=cht_id,
                         text=msg_kyb[0]["message"],
                         reply_markup=msg_kyb[0]["keyboard"])
        msg_key = 0


def calculate_price(chat_id):
    global usr_dat
    lit = str(red.hget(chat_id, 'amount'), encoding='utf-8')
    pdc = str(red.hget(chat_id, 'product'), encoding='utf-8')

    prc = 15000
    prc += int(lit) * session.query(Product).filter(Product.title == pdc).first().unit_price

    return prc


def send_success_pay(chat_id, message_id):
    customer = Customer(chat_id=chat_id,
                        name=str(red.hget(chat_id, 'name'), encoding='utf-8'),
                        phone=str(red.hget(chat_id, 'phone'), encoding='utf-8'),
                        longitude=float(red.hget(chat_id, 'longitude')),
                        latitude=float(red.hget(chat_id, 'latitude')),
                        address=str(red.hget(chat_id, 'address'), encoding='utf-8'))
    session.add(customer)

    order = Order(customer_id=chat_id,
                  product_id=session.query(Product).filter(
                      Product.title == str(red.hget(chat_id, 'product'), encoding='utf-8')).first().product_id,
                  amount=int(red.hget(chat_id, 'amount')),
                  cost=calculate_price(chat_id),
                  order_date=str(red.hget(chat_id, 'date'), encoding='utf-8'),
                  order_time=str(red.hget(chat_id, 'time'), encoding='utf-8') + " " +
                             str(red.hget(chat_id, 'period'), encoding='utf-8')
                  )
    session.add(order)
    session.commit()

    msg_key = 10
    bot.edit_message_reply_markup(chat_id=chat_id,
                                  message_id=message_id,
                                  reply_markup=None)

    bot.send_message(chat_id=chat_id,
                     text=msg_kyb["pay_success"]["message"],
                     reply_markup=msg_kyb["pay_success"]["keyboard"])

    red.delete(chat_id)


def send_failed_pay(chat_id, message_id):
    red.delete(chat_id)

    msg_key = 10
    bot.edit_message_reply_markup(chat_id=chat_id,
                                  message_id=message_id,
                                  reply_markup=None)

    bot.send_message(chat_id=chat_id,
                     text=msg_kyb["pay_failed"]["message"],
                     reply_markup=msg_kyb["pay_failed"]["keyboard"])


if __name__ == '__main__':
    bot.threaded = True
    bot.polling(none_stop=True, timeout=0)
