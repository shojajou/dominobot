from telebot.types import *
from khayyam.jalali_datetime import *
from database import *
from models import *

# ret_kyb_but = KeyboardButton(text="بازگشت")
fst_kyb_but = KeyboardButton(text="آغاز دوباره")

fst_inl_kyb_but = InlineKeyboardButton(text="آغاز دوباره", callback_data="restart")
pos_kyb_but = InlineKeyboardButton(text="بله", callback_data="order_accepted")
neg_kyb_but = InlineKeyboardButton(text="خیر", callback_data="order_denied")

rep_mrk_0 = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_mrk_0.add(
    KeyboardButton(text="کالاها")
)

rep_mrk_1_keys = [p[0] for p in session.query(Product.title).all()]
rep_mrk_1 = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_mrk_1.add(*rep_mrk_1_keys)
rep_mrk_1.row(fst_kyb_but)

rep_mrk_2_keys = ["25", "30", "35", "40", "45", "50"]
rep_mrk_2 = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=3)
rep_mrk_2.add(*rep_mrk_2_keys)
rep_mrk_2.row(fst_kyb_but)

rep_mrk_3 = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_mrk_3.add(
    KeyboardButton(text="لوکشینم رو بفرست", request_location=True)
)
rep_mrk_3.row(fst_kyb_but)

rep_mrk_4 = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_mrk_4.row(fst_kyb_but)

rep_mrk_5 = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_mrk_5.row(fst_kyb_but)

rep_mrk_6 = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_mrk_6.row(fst_kyb_but)

rep_mrk_7_keys = [(JalaliDatetime.now() + timedelta(days=day_inc)).strftime("%A %d %B %y") for day_inc in range(0, 2)]
rep_mrk_7 = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_mrk_7.add(*rep_mrk_7_keys)
rep_mrk_7.row(fst_kyb_but)

rep_time_periods_mrk_8 = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_time_periods_mrk_8.add("قبل از ظهر", "بعد از ظهر")
rep_time_periods_mrk_8.row(fst_kyb_but)

rep_8_times_dic = {
    "قبل از ظهر": ["5 - 6", "6 - 7", "7 - 8", "8 - 9", "9 - 10", "10 - 11", "11 - 12"],
    "بعد از ظهر": ["1 - 2", "2 - 3", "3 - 4", "4 - 5", "5 - 6", "6 - 7", "7 - 8", "8 - 9", "9 - 10"]
}

rep_mrk_8_am = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_mrk_8_am.add(*rep_8_times_dic["قبل از ظهر"])
rep_mrk_8_am.row(fst_kyb_but)

rep_mrk_8_pm = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
rep_mrk_8_pm.add(*rep_8_times_dic["بعد از ظهر"])
rep_mrk_8_pm.row(fst_kyb_but)

rep_mrk_9 = InlineKeyboardMarkup(row_width=2)
rep_mrk_9.row(pos_kyb_but, neg_kyb_but)

rep_mrk_10 = InlineKeyboardMarkup(row_width=2)
rep_mrk_10.row(InlineKeyboardButton(text="پرداخت", url="http://5.9.227.56:8004/request/", callback_data="pay_zarinpal"))
rep_mrk_10.row(InlineKeyboardButton(text="انصراف", callback_data="cancel_order"))

msg_kyb = {
    0: {
        "keyboard": rep_mrk_0,
        "message": """
                   سلام به بات دومینو خوش آمدید.
                   
                   @dominoenergybot
                   """
    },
    1: {
        "keyboard": rep_mrk_1,
        "message": """
                   نوع سوخت رو مشخص کن.
                   
                   @dominoenergybot
                   """,
        "prev_key": 0
    },
    2: {
        "keyboard": rep_mrk_2,
        "message": """
                   حجم لیتر درخواستی رو مشخص کن.
                   
                   @dominoenergybot
                   """,
        "prev_key": 1
    },
    3: {
        "keyboard": rep_mrk_3,
        "message": """
                   لطفا لوکیشنت رو برام بفرست.
                   
                   @dominoenergybot
                   """,
        "prev_key": 2
    },
    4: {
        "keyboard": rep_mrk_4,
        "message": """
                   لطفا آدرس دقیقت رو برام بفرست.
                   
                   @dominoenergybot
                   """,
        "prev_key": 3
    },
    5: {
        "keyboard": rep_mrk_5,
        "message": """
                   لطفا نامت رو وارد کن.
                   
                   @dominoenergybot
                   """,
        "prev_key": 4
    },
    6: {
        "keyboard": rep_mrk_6,
        "message": """
                   لطفا شماره تماسی وارد کنید.
                   
                   @dominoenergybot
                   """,
        "prev_key": 5
    },
    7: {
        "keyboard": rep_mrk_7,
        "message": """
                   لطفا روز دریافت سفارش رو از میان کلیدهای زیر انتخاب کن.
                   
                   @dominoenergybot
                   """,
        "prev_key": 6
    },
    8: {
        "keyboard": rep_time_periods_mrk_8,
        "message": """
                   لطفا ساعت دریافت سفارش رو انتخاب کن.
                   
                   @dominoenergybot
                   """,
        "prev_key": 7
    },
    9: {
        "keyboard": rep_mrk_9,
        "message": """
        کاربر گرامی، سفارش شما با اطلاعات زیر دریافت شد:

⛽️    ️نوع سوخت: {0}
🛢    مقدار(به لیتر): {1}
👤    نام گیرنده: {2}
☎️    شماره تماس: {3}
📅    روز دریافت: {4}
⏰    ساعت دریافت: {5}
📍    آدرس گیرنده: {6}
           
❗️ آیا به ثبت نهایی درخواست تمایل دارید؟
        
        @dominenergybot
        """,
        "prev_key": 8
    },
    10: {
        "keyboard": rep_mrk_10,
        "message": """
        کاربر گرامی، سفارش شما با اطلاعات زیر ثبت شد:
        
⛽️    ️نوع سوخت: {0}
🛢    مقدار(به لیتر): {1}
👤    نام گیرنده: {2}
☎️    شماره تماس: {3}
📅    روز دریافت: {4}
⏰    ساعت دریافت: {5}
📍    آدرس گیرنده: {6}
        
💰    جمع پرداخت: {7}
    
✳️    کاربر گرامی، در جمع پرداختی شما مقدار 15000 تومان به عنوان هزینه ارسال در نظر گرفته شده است.
        
برای پرداخت روی دکمه پرداخت کلیک/لمس کنید.
        
        @dominenergybot
        """,
        "prev_key": 9
    },
    "cancel_order": {
        "keyboard": None,
        "message": """
        سفارش شما بنا به درخواست انصراف شما حذف شد.
برای آغاز دوباره روی /restart کلیک/لمس کنید.
        
        @dominenergybot
        """,
        "prev_key": 10
    },
    "error_data": {
        "keyboard": rep_mrk_6,
        "message": """
        متاسفانه در دریافت اطلاعات اشکالی پیش آمده است.
لطفا برای آغاز مجدد روی کلید زیر کلیک/لمس کنید.
        
        @dominoenergybot
        """
    },
    "phone_error": {
        "keyboard": None,
        "message": """
        لطفا در وارد کردن شماره تماس دقت فرمایید.
توجه: شماره تماس باید به صورت عدد و دقیقا 11 رقم باشد. به مثال‌های زیر توجه کنید.
        
        02112345678
        یا
        09121234567
        """
    },
    "pay_success": {
        "keyboard": rep_mrk_6,
        "message": """
        کاربر گرامی پرداخت شما با موفقیت انجام شد.
محصول شما در زمان تعیین شده توسط شما برای شما ارسال خواهد شد.
از خرید شما متشکریم.
        
        @dominoenergybot
        """
    },
    "pay_failed": {
        "keyboard": rep_mrk_6,
        "message": """
        عملیات پرداخت ناموفق بود.
سفارش شما از سیستم حذف شده است.
برای انجام سفارش جدید روی دکمه آغاز دوباره کلیک/لمس کنید.
        
        @dominoenergybot
        """
    }
}
