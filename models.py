from sqlalchemy import *
from sqlalchemy.orm import mapper
from khayyam import JalaliDatetime
import os
from database import meta


"""
    --------------------STATE--------------------
"""
status = Table('status', meta,
               Column('state_id', Integer, primary_key=True, nullable=False),
               Column('state', UnicodeText(100)),
               )


class State(object):
    def __init__(self, state):
        self.state = state


"""
    --------------------CUSTOMER--------------------
"""
customers = Table('customers', meta,
                  Column('customer_id', BigInteger, primary_key=True, nullable=False),
                  Column('chat_id', BigInteger, nullable=False),
                  Column('name', UnicodeText(100), nullable=False),
                  Column('phone', String(11), nullable=False),
                  Column('longitude', Float, nullable=False),
                  Column('latitude', Float, nullable=False),
                  Column('address', UnicodeText, nullable=False),
                  )


class Customer(object):
    def __init__(self, chat_id, name, phone, longitude, latitude, address):
        self.customer_id = int.from_bytes(os.urandom(4), byteorder="big")
        self.chat_id = chat_id
        self.name = name
        self.phone = phone
        self.longitude = longitude
        self.latitude = latitude
        self.address = address


"""
    --------------------PRODUCT--------------------
"""
products = Table('products', meta,
                 Column('product_id', Integer, primary_key=True, nullable=False),
                 Column('title', String(100), nullable=False),
                 Column('unit_price', BigInteger, nullable=False),
                 )


class Product(object):
    def __init__(self, title, unit_price):
        self.title = title,
        self.unit_price = unit_price


"""
    --------------------ORDER--------------------
"""
orders = Table('orders', meta,
               Column('order_id', BigInteger, primary_key=True, nullable=False),
               Column('state_id', Integer, ForeignKey('status.state_id'), nullable=False, default=1),
               Column('customer_id', BigInteger, ForeignKey('customers.customer_id'), nullable=False),
               Column('product_id', Integer, ForeignKey('products.product_id'), nullable=False),
               Column('amount', Integer, nullable=False),
               Column('cost', BigInteger, nullable=False),
               Column('current_date', DateTime, nullable=False),
               Column('order_date', UnicodeText, nullable=False),
               Column('order_time', UnicodeText, nullable=False)
               )


class Order(object):
    def __init__(self, customer_id, product_id, amount, cost, order_date, order_time, state_id=1):
        self.order_id = int.from_bytes(os.urandom(4), byteorder="big")
        self.state_id = state_id
        self.customer_id = customer_id
        self.product_id = product_id
        self.amount = amount
        self.cost = cost
        self.current_date = JalaliDatetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.order_date = order_date
        self.order_time = order_time


mapper(State, status)
mapper(Product, products)
mapper(Customer, customers)
mapper(Order, orders)

