# Usage
>**Before start, please make sure you activated your python [virtual environment](http://docs.python-guide
.org/en/latest/dev/virtualenvs/), then start.**
## Installing Requirements
```bash
$ cd /path/to/dominobot
$ pip install -r requirements.txt
```

## Initializing Database
First of all you should create a database and a user which has access to it:

Login into mysql as super user and enter password
```bash
$ mysql -u root -p
```

Create a database which its character set is `utf8mb4`
```mysql
CREATE DATABASE dominodb CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```

Set suitable privileges
```mysql
GRANT ALL PRIVILEGES ON dominodb.* TO '<username>'@'localhost' IDENTIFIED BY '<password>';
FLUSH PRIVILEGES;
```

Set username and password in `database.py` file like below:
> We decided to use mysql engine
```python
engine = create_engine("mysql://<username>:<password>@localhost/dominodb", convert_unicode=True)
```

Then move to project directory:
```bash
$ cd /path/to/dominobot
```

Then open a Python interpreter and run following:
```pythonstub
>>> from database import init_db
>>> init_db()
```
An alternative way is:
```bash
$ python -c "from database import init_db; init_db()"
```

## Running Message Handler
```bash
$ cd /path/to/dominobot
$ python message_handler.py
```

## Running ZarinPal Doorway
### Python itself
```bash
$ cd /path/to/dominobot
$ python
```

### Gunicorn
```bash
$ cd /path/to/dominobot
$ gunicorn -w <workers_count> -b <ip:port> zarinpal:app
```

# Bot ID
https://t.me/dominoenergybot
