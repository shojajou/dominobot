from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import *

engine = create_engine("mysql://domino:1234@localhost/dominodb?charset=utf8mb4", convert_unicode=True)
session = scoped_session(sessionmaker(bind=engine, autoflush=False, autocommit=False))
meta = MetaData(engine)


def init_db():
    from models import status, products, customers, orders
    meta.create_all(bind=engine, tables=[status, products, customers, orders])


def drop_all():
    from models import status, products, customers, orders
    meta.drop_all(bind=engine, tables=[status, products, customers, orders])


def drop_table(model):
    model.__table__.drop(bind=engine)

